const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const app = express().use(bodyParser.json());

// Add headers
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader("Access-Control-Allow-Origin", "*");

    // Request methods you wish to allow
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    );

    // Request headers you wish to allow
    res.setHeader(
        "Access-Control-Allow-Headers",
        "X-Requested-With,content-type"
    );

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader("Access-Control-Allow-Credentials", true);

    // Pass to next layer of middleware
    next();
});

//Setup connection string
const con = mysql.createConnection({
    host: "localhost",
    user: "amanda",
    password: "amanda",
    database:"puhelinluettelo",
    multipleStatements:true
});

//connection to db
con.connect((err) => {
    if (err) {
        console.log("Error connecting to Db");
        return;
    }
    console.log("Connection established");
});

app.get("/people", (req, res) => {
    //hakee puhelinnumeroita
    con.query("SELECT * FROM henkilot", (err, rows) => {
        if (err) throw err;
        let data = [];
        rows.forEach((row) => {
            let tiedot = {
                id:row.id,
                nimi:row.nimi,
                puhelin:row.puhelin
            }
           data.push(tiedot);
        });
        res.json(data);
    });
});

app.post("/people", (req, res) => {
    //lisää puhelinnumeroita
    const henkilo = req.body;
    con.query('INSERT INTO henkilot SET ?', henkilo, (err, dbres) => {
        if(err) throw err;
        con.query("SELECT * FROM henkilot WHERE ID = ?",dbres.insertId, (err, rows) => {
            if (err) throw err;
            let data = [];
            rows.forEach((row) => {
                let tiedot = {
                    id:row.id,
                    nimi:row.nimi,
                    puhelin:row.puhelin
                }
                data.push(tiedot);
            });
            res.status(201);
            res.json(data);
        });
    });
});

app.put("/people/:id", (req, res) => {
    //päivittää tietoja
    const id = Number(req.params.id);
    const henkilo = req.body;
    con.query('UPDATE henkilot SET nimi = ?, puhelin = ? Where ID = ?', [henkilo.nimi, henkilo.puhelin, id], (err, result) => {
            if (err) throw err;
        con.query("SELECT * FROM henkilot WHERE ID = ?",id, (err, rows) => {
            if (err) throw err;
            let data = [];
            rows.forEach((row) => {
                let tiedot = {
                    id:row.id,
                    nimi:row.nimi,
                    puhelin:row.puhelin
                }
                data.push(tiedot);
            });
            res.json(data);
        });
    });
});

app.delete("/people/:id", (req, res) => {
    //poistetaan tietoja
    const id = Number(req.params.id);
    con.query('DELETE FROM henkilot WHERE id = ?', [id], (err, result) => {
        if (err) throw err;
        });
    res.end();
    });

app.listen(3000, () => {
    console.log("Server listening at port 3000");
});