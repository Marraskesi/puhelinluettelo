const mysql = require("mysql");
const con = mysql.createConnection({
    host: "localhost",
    user: "amanda",
password: "amanda",
database:"puhelinluettelo",
    multipleStatements:true
});
con.connect((err) => {
    if (err) {
        console.log("Error connecting to Db");
        return;
    }
    console.log("Connection established");
});

const henkilo = { nimi: 'Ankka Roope', puhelin: '050-1231232' };
con.query('INSERT INTO henkilot SET ?', henkilo, (err, res) => {
    if(err) throw err;
    console.log('Last insert ID:', res.insertId);
});

con.query(
    'UPDATE henkilot SET puhelin = ? Where ID = ?',
    ['044-6544655', 3],
    (err, result) => {
        if (err) throw err;
        console.log(`Changed ${result.changedRows} row(s)`);
    }
);

con.query(
    'DELETE FROM henkilot WHERE id = ?', [5], (err, result) => {
        if (err) throw err;
        console.log(`Deleted ${result.affectedRows} row(s)`);
    }
);

con.query("SELECT * FROM henkilot", (err, rows) => {
    if (err) throw err;
    console.log("Data received from Db:");
    rows.forEach((row) => {
        console.log(`${row.nimi}, puhelin on ${row.puhelin}`);
    });
});

con.query(
    "SET @henkilo_id = 0; CALL sp_insert_henkilo(@henkilo_id, 'Matti Miettinen', '044-5431232'); SELECT @henkilo_id",
(err, rows) => {
    if (err) throw err;
    console.log('Data received from Db:\n');
    console.log(rows);
}
);

con.query("CALL sp_get_henkilot()", function (err, rows) {
    if (err) throw err;
    console.log("Data received from Db:");
    rows[0].forEach( (row) => {
        console.log(`${row.nimi}, puhelin: ${row.puhelin}`);
    });
});

const userSubmittedVariable = "1"; /*että kukaan ei voi syöttää tätä:
const userSubmittedVariable = '1; DROP TABLE henkilot';*/
con.query(
    `SELECT * FROM henkilot WHERE id = ${mysql.escape(userSubmittedVariable)}`,
    (err, rows) => {
        if (err) throw err;
        console.log(rows);
    }
);

con.end((err) => {
// The connection is terminated gracefully
// Ensures all remaining queries are executed
// Then sends a quit packet to the MySQL server.
});

